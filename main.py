import csv

hosts_list = list()
dns = open("dns1.log")
hosts = open("hosts1.txt")
meta = csv.reader(dns, delimiter="\x09")
hosts_csv = csv.reader(hosts)

for row in hosts_csv:
    if len(row) > 0:
        hosts_list.append(row[0])
hosts.close()
counter = 0
for row in meta:
    try:
        if row[8] in hosts_list:
            counter += 1
            print("Хост: " + row[8] + " нежелателен!")
    except IndexError:
        pass
print("Нежелательный трафик: " + str(round(counter / (meta.line_num - 8) * 100, 2)) + "%")
dns.close()